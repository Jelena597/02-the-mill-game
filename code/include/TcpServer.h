#ifndef TCPSERVER_H
#define TCPSERVER_H

#include <QTcpServer>
#include <QWidget>

class QTcpServer;
class QTcpSocket;

class TcpServer : public QWidget {
    Q_OBJECT

public:
    explicit TcpServer(QWidget* parent = nullptr);

    ~TcpServer() override;

private slots:
    void newConnection();
    void removeConnection();
    void newMessage();

private:
    QTcpServer* m_server;
    QList<QTcpSocket*> m_clients;
    QHash<QTcpSocket*, QByteArray> m_receivedData;
    TcpServer(const TcpServer&) = delete;
    TcpServer& operator=(const TcpServer&) = delete;
    TcpServer(TcpServer&&) = delete;
    TcpServer& operator=(TcpServer&&) = delete;
};
#endif // TCPSERVER_H
