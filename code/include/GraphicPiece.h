#ifndef GRAPHICPIECE_H
#define GRAPHICPIECE_H

#include "Lib.h"
#include "QGraphicsItem"
#include <QCoreApplication>

/*
 * Class for drawing fields and pieces on the board
 */
class GraphicPiece : public QGraphicsItem {
public:
    void set_player(FIELDSTATE p);

    QRectF boundingRect() const override;

    explicit GraphicPiece(QGraphicsItem* parent = nullptr);

    explicit GraphicPiece(FIELDSTATE player, QGraphicsItem* parent = nullptr);

    void paint(QPainter* painter, const QStyleOptionGraphicsItem* option,
               QWidget* widget) override;

private:
    FIELDSTATE m_player;
};
#endif // GRAPHIC_PIECE_H
