#ifndef MYGRAPHICSSCENE_H
#define MYGRAPHICSSCENE_H

#include "QGraphicsScene"
#include <QPushButton>

class MyGraphicsScene : public QGraphicsScene {
    Q_OBJECT

signals:
    void signalClickedSomething(QPointF);

protected:
    virtual void mousePressEvent(QGraphicsSceneMouseEvent* mouseEvent) override;

public:
    MyGraphicsScene();
    ~MyGraphicsScene();

    int getRedPieces();
    int getBluePieces();
    void setRedPieces(int value);
    void setBluePieces(int value);

    void decrementRedPieces();
    void decrementBluePieces();

private:
    int redPieces;
    int bluePieces;
    MyGraphicsScene(const MyGraphicsScene&) = delete;
    MyGraphicsScene& operator=(const MyGraphicsScene&) = delete;
    MyGraphicsScene(MyGraphicsScene&&) = delete;
    MyGraphicsScene& operator=(MyGraphicsScene&&) = delete;
};
#endif // MYGRAPHICSVIEW_H
