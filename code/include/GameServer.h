#ifndef GAMESERVER_H
#define GAMESERVER_H

#include "Game.h"
#include "GameMap.h"
#include "Lib.h"
#include "TcpClient.h"
#include <QObject>
#include <QWidget>

class GameServer : public QWidget, public Game {
    Q_OBJECT
public:
    GameServer(QWidget* parent, TcpClient* p1, TcpClient* p2, GAMEMODE gameMode);

    ~GameServer() override;

    void play();
    void playMove(Player* player, int index, MyGraphicsScene* scene) override;
    void sendMoveToServer(GAMEMOVE move, int fromIndex, int toIndex);

public slots:
    void readMoveFromServer(GAMEMOVE move);

public:
    FIELDSTATE active;

private:
    GameServer(const GameServer&) = delete;
    GameServer& operator=(const GameServer&) = delete;
    GameServer(GameServer&&) = delete;
    GameServer& operator=(GameServer&&) = delete;
};
#endif // GAMESERVER_H
