#include "code/include/Board.h"
#include "code/include/Game.h"
#include "code/include/MainMenu.h"
#include <QApplication>

int main(int argc, char* argv[]) {
    QApplication a(argc, argv);

    MainMenu menu;

    menu.show();
    return a.exec();
}
